package com.streamershark.game;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class GameScreen implements Screen {
    final StreamerSharkGame game;

    Texture dropImage;
    Texture playerImage;
    Texture likeImg;
    Texture subImg;
    Sound dropSound;
    Music backMusic;
    Sound lifedec;
    OrthographicCamera camera;
    Rectangle player;
    Array<Rectangle> drops;
    Array<Rectangle> likes;
    Array<Rectangle> subs;
    Array<Rectangle> rectangles;
    long lastDropTime;
    int dropsGathered;
    long gameTime;
    float aspectRatio = (float)Gdx.graphics.getWidth()/Gdx.graphics.getHeight();
    float speedUp = 300;
    private float previousY;
    int lastY = 200;
    int spawned = 0;
    int randomNum;
    int lives = 5;

    public GameScreen(final StreamerSharkGame gam) {
        this.game = gam;

        // загрузка изображений для капли и ведра, 64x64 пикселей каждый

        dropImage = new Texture(Gdx.files.internal("donation.png"));
        likeImg = new Texture(Gdx.files.internal("like.png"));
        subImg = new Texture(Gdx.files.internal("sub.png"));
        playerImage = new Texture(Gdx.files.internal("shark.png"));
        playerImage.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        // загрузка звукового эффекта падающей капли и фоновой "музыки" дождя
        dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"));
        backMusic = Gdx.audio.newMusic(Gdx.files.internal("backsound.mp3"));
        backMusic.setLooping(false);
        lifedec = Gdx.audio.newSound(Gdx.files.internal("lifedec.wav"));

        // создает камеру
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 800/aspectRatio);

        // создается Rectangle для представления ведра
        player = new Rectangle();

        player.width = 128;
        player.height = 128;
        // центрируем ведро по горизонтали
        player.x = 800 / 2 - 128 / 2;
        // размещаем на 20 пикселей выше нижней границы экрана.
        player.y = 0;


        // создает массив капель и возрождает первую
        drops = new Array<Rectangle>();
        likes = new Array<Rectangle>();
        subs = new Array<Rectangle>();
        rectangles = new Array<Rectangle>();
        //spawnDrop();
        gameTime = TimeUtils.nanosToMillis(TimeUtils.nanoTime());
    }

    private void spawnDrop() {
        Rectangle drop = new Rectangle();
        drop.x = MathUtils.random(0, 800 - 64);
        drop.y = 480;
        drop.width = 64;
        drop.height = 64;
        randomNum = (1 + (int) (Math.random() * 3));
        switch (randomNum){
            case 1:
                likes.add(drop);
                break;
            case 2:
                drops.add(drop);
                break;
            case 3:
                subs.add(drop);
                break;
        }
        rectangles.add(drop);
        lastDropTime = TimeUtils.nanoTime();
    }



    @Override
    public void render(float delta) {
        // очищаем экран темно-синим цветом.
        // Аргументы для glClearColor красный, зеленый
        // синий и альфа компонент в диапазоне [0,1]
        // цвета используемого для очистки экрана.
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // сообщает камере, что нужно обновить матрицы.
        camera.update();

        // сообщаем SpriteBatch о системе координат
        // визуализации указанных для камеры.
        game.batch.setProjectionMatrix(camera.combined);

        // начитаем новую серию, рисуем ведро и
        // все капли

        game.batch.begin();
        game.batch.draw(new Texture(Gdx.files.internal("bg.jpg")),0,0,800,480);
        game.font.draw(game.batch, "Points: " + dropsGathered, 15, 800/aspectRatio-10);
        game.font.draw(game.batch, "Lives: " + lives, 240, 800/aspectRatio-10);
        game.font.draw(game.batch, "Time: " + ((TimeUtils.nanosToMillis(TimeUtils.nanoTime())-gameTime)/1000), 480, 800/aspectRatio-10);
        //верхнюю строку с вычислениями надо будет отрефакторить, но оно хотя бы работает, ЮХУ!
        game.batch.draw(playerImage, player.x, player.y,128,128);
        for (Rectangle raindrop : drops) {
            game.batch.draw(dropImage, raindrop.x, raindrop.y);
        }
        for (Rectangle like : likes) {
            game.batch.draw(likeImg, like.x, like.y);
        }
        for (Rectangle sub : subs) {
            game.batch.draw(subImg, sub.x, sub.y);
        }
        game.batch.end();



        // обработка пользовательского ввода
        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            player.x = touchPos.x - 128 / 2;
        }
        if (Gdx.input.isKeyPressed(Keys.LEFT))
            player.x -= 200 * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isKeyPressed(Keys.RIGHT))
            player.x += 200 * Gdx.graphics.getDeltaTime();

        // убедитесь, что ведро остается в пределах экрана
        if (player.x < 0)
            player.x = 0;
        if (player.x > 800 - 128)
            player.x = 800 - 128;

        //проверка, нужно ли создавать новую каплю
        if (previousY < lastY){
            spawnDrop();
            spawned++;
            if (spawned == 4) {
                lastY = lastY + 1;
                spawned = 0;
            }
        }

        // движение капли, удаляем все капли выходящие за границы экрана
        // или те, что попали в ведро. Воспроизведение звукового эффекта
        // при попадании.

        Iterator<Rectangle> iter = rectangles.iterator();

        while (iter.hasNext()) {
            Rectangle raindrop = iter.next();
            raindrop.y -= speedUp * Gdx.graphics.getDeltaTime();
            speedUp += delta * 3;
            previousY = raindrop.y;
            if (raindrop.y + 64 < 0){
                iter.remove();
                likes.removeValue(raindrop, true);//identity - тип сравнения(== или equals())
                drops.removeValue(raindrop, true);
                subs.removeValue(raindrop, true);
                lives--;
                lifedec.play();
            }
            if (raindrop.overlaps(player)) {
                dropsGathered++;
                if (likes.contains(raindrop, true)){
                    dropsGathered = dropsGathered + 4;
                }
                if (subs.contains(raindrop, true)){
                    dropsGathered = dropsGathered + 9;
                }
                dropSound.play();
                iter.remove();
                likes.removeValue(raindrop, true);
                drops.removeValue(raindrop, true);
                subs.removeValue(raindrop, true);
            }

        }
        if ((TimeUtils.nanosToMillis(TimeUtils.nanoTime()) - gameTime > 130000) || lives == 0){
            game.setScreen(new ScoreScreen(game, dropsGathered));
            backMusic.stop();
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        // воспроизведение фоновой музыки
        // когда отображается экрана
        backMusic.play();
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        dropImage.dispose();
        likeImg.dispose();
        subImg.dispose();
        playerImage.dispose();
        dropSound.dispose();
        backMusic.dispose();
        lifedec.dispose();
    }

}
