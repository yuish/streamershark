package com.streamershark.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

class MainMenuScreen implements Screen {

    final StreamerSharkGame game;
    OrthographicCamera camera;
    float aspectRatio = (float)Gdx.graphics.getWidth()/Gdx.graphics.getHeight();
    GlyphLayout startGlyph;
    MenuController controller;


    public MainMenuScreen(final StreamerSharkGame gam) {
        game = gam;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 800/aspectRatio);
    }

    @Override
    public void show() {
        startGlyph = new GlyphLayout(game.font, "Tap here to start!");

        controller = new MenuController(startGlyph);
        Gdx.input.setInputProcessor(controller);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(new Texture(Gdx.files.internal("bg.jpg")),0,0,800,480);
        game.font.draw(game.batch, "WELCOME TO STREAMER SHARK GAME", 140, 800/aspectRatio/2);
        game.font.draw(game.batch, startGlyph, 300, 800/aspectRatio/2-30);
        game.batch.end();

        if (controller.touchDown(Gdx.input.getX() , (int) (Gdx.input.getY()))) {
            game.setScreen(new GameScreen(game));
            dispose();
        }
/*
        if (Gdx.input.isTouched()){
            game.setScreen(new GameScreen(game));
            dispose();
        }*/
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
