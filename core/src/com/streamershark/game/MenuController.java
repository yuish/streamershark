package com.streamershark.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

public class MenuController implements InputProcessor {

    private GlyphLayout layout;

    double convX = (double)Gdx.graphics.getWidth() / 800 ;
    double convY = (double) Gdx.graphics.getHeight()/ 800;

    public MenuController (GlyphLayout layout){
        this.layout = layout;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    public boolean touchDown(int screenX, int screenY) {
        if (screenX/ convX >= 290 && screenX / convX <= 503 && screenY/ convY >= 430 && screenY / convY <= 496 && Gdx.input.isTouched()){
            System.out.println(screenX/convX +"_"+ screenY/convY +"_" + convX);
            return true;
        }else {
            if (Gdx.input.isTouched()){
                System.out.println(screenX/convX +"_"+ screenY/convY + "_" + convX +"_" + Gdx.graphics.getWidth());
            }
            return false;
        }
    }
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
