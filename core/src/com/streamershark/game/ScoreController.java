package com.streamershark.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

public class ScoreController implements InputProcessor {

    private double convX = (double)Gdx.graphics.getWidth() / 800 ;
    private double convY = (double) Gdx.graphics.getHeight()/ 800;

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    public boolean touchDownAgain(int screenX, int screenY) {
        if (screenX/ convX >= 243 && screenX / convX <= 370 && screenY/ convY >= 463 && screenY / convY <= 519 && Gdx.input.isTouched()){
            return true;
        }else {
            return false;
        }
    }

    public boolean touchDownToMenu(int screenX, int screenY) {
        if (screenX/ convX >= 245 && screenX / convX <= 421 && screenY/ convY >= 520 && screenY / convY <= 640 && Gdx.input.isTouched()){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
