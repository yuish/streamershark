package com.streamershark.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.TimeUtils;

public class ScoreScreen implements Screen {
    StreamerSharkGame game;
    OrthographicCamera camera;
    float aspectRatio = (float) Gdx.graphics.getWidth()/Gdx.graphics.getHeight();
    int score;
    GlyphLayout tryagainGlyph;
    GlyphLayout backToMenuGlyph;
    ScoreController controller;
    Music scoreMusic;
    long gameTime;



    public ScoreScreen(StreamerSharkGame game, int dropsGathered) {
        this.game = game;
        this.score = dropsGathered;

        scoreMusic = Gdx.audio.newMusic(Gdx.files.internal("endgame.mp3"));
        scoreMusic.play();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 800/aspectRatio);
        gameTime = TimeUtils.nanosToMillis(TimeUtils.nanoTime());

    }

    @Override
    public void show() {
        tryagainGlyph = new GlyphLayout(game.font, "Try again");
        backToMenuGlyph = new GlyphLayout(game.font, "Back to menu");


        controller = new ScoreController();
        Gdx.input.setInputProcessor(controller);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(new Texture(Gdx.files.internal("bg.jpg")),0,0,800,480);
        game.font.draw(game.batch, "GAME OVER", 250, 800/aspectRatio/2 + 80);
        game.font.draw(game.batch, "Your score: " + score, 250, 800/aspectRatio/2-10);
        game.font.draw(game.batch, tryagainGlyph,  250, 800/aspectRatio/2-50);
        game.font.draw(game.batch, backToMenuGlyph,  250, 800/aspectRatio/2-90);
        game.batch.end();


        if (controller.touchDownAgain(Gdx.input.getX(), Gdx.input.getY()) && (TimeUtils.nanosToMillis(TimeUtils.nanoTime()) - gameTime) > 1000) {
            game.setScreen(new GameScreen(game));
            dispose();
        }
        if (controller.touchDownToMenu(Gdx.input.getX(), Gdx.input.getY()) && (TimeUtils.nanosToMillis(TimeUtils.nanoTime()) - gameTime) > 1000){
            game.setScreen(new MainMenuScreen(game));
            dispose();
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        scoreMusic.dispose();
    }
}
